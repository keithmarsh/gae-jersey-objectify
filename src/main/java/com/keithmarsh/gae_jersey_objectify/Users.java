package com.keithmarsh.gae_jersey_objectify;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.googlecode.objectify.ObjectifyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value="/user")
public class Users {
    Logger mLog = Logger.getLogger(this.getClass().getName());
    static {
        ObjectifyService.register(User.class);
    }

    @GET @Path("{id}")
    @ApiOperation(response=User.class, value = "Get a user")
    @ApiResponses(value = { @ApiResponse(code=200, message = "OK") })
    public User getUser(@PathParam("id") @ApiParam() String id) {
        User u = ofy().load().type(User.class).id(id).now();
        return u;
    }
    
    @POST
    public User postUser(User u) {
        ofy().save().entity(u).now();
        return u;
    }
}

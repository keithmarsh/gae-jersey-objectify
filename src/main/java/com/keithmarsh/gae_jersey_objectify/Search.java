package com.keithmarsh.gae_jersey_objectify;

import java.util.Date;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class Search {
	@Id
	Long id;
	@Parent
	Ref<User> owner;
	int category;
	String keywords;
	float maxPrice;
	Date latest;
	
	public Search() {
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public User getOwner() {
		return owner.get();
	}
	public void setOwner(User owner) {
		this.owner = Ref.create(owner);
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public float getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(float maxPrice) {
		this.maxPrice = maxPrice;
	}
	public Date getLatest() {
		return latest;
	}
	public void setLatest(Date latest) {
		this.latest = latest;
	}

    @Override
    public String toString() {
        return "Search [id=" + id + ", owner=" + owner + ", category=" + category + ", keywords=" + keywords
                + ", maxPrice=" + maxPrice + ", latest=" + latest + "]";
    }
}

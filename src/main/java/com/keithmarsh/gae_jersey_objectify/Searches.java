package com.keithmarsh.gae_jersey_objectify;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.googlecode.objectify.ObjectifyService;

@Path("search")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Searches {
    Logger mLog = Logger.getLogger(this.getClass().getName());
    
    static {
        ObjectifyService.register(Search.class);
        ObjectifyService.register(User.class);
    }

    @GET @Path("{id}")
    public Search getSearch(@PathParam("id") long id) {
        mLog.info("id " + id);
        Search s = ofy().load().type(Search.class).id(id).now();
        return s;
    }
    
    @POST
    public Search postSearch(Search search) {
        ofy().save().entity(search).now();
        return search;
    }
}
A work-in-progress example of using JAX-WS-RS (Jersey), Objectify and Swagger.

You can POST to /user with a body of { "id" : "yourname" } and GET /user/yourname.

View the swagger at http://localhost:8888/swagger.json or .yaml

Build and run using gradle appenginerun